package ni

import (
	"bytes"
	"testing"
)

var (
	testURL        = "ni:///sha-256;UyaQV-Ev4rdLoHyJJWCi11OHfrYv9E1aGQAlMO2X_-Q"
	testNIH        = "nih:1;532690-57e12f-e2b74b-a07c89-2560a2-d75387-7eb62f-f44d5a-190025-30ed97-ffe4;0"
	testNIHInvalid = "nih:1;532690-57e12f-e2b74b-a07c89-2560a2-d75387-7eb62f-f44d5a-190025-30ed97-ffe4;1"
)

func TestParse(t *testing.T) {
	url, err := ParseNI(testURL)
	if err != nil {
		t.Fatal(err)
	}

	if url.Authority != "" {
		t.FailNow()
	}

	if url.Alg != "sha-256" {
		t.FailNow()
	}

	if url.Val != "UyaQV-Ev4rdLoHyJJWCi11OHfrYv9E1aGQAlMO2X_-Q" {
		t.FailNow()
	}

	if url.suiteID != 0x01 {
		t.FailNow()
	}

	// TODO: test valbytes
}

func TestParseNIH(t *testing.T) {
	url, err := ParseNIH(testNIH)
	if err != nil {
		t.Fatal(err)
	}

	if url.String() != testURL {
		t.FailNow()
	}
}

func TestParseNIHInvalid(t *testing.T) {
	_, err := ParseNIH(testNIHInvalid)
	if err == nil {
		t.Fatal(err)
	}
}

func TestWellKnown(t *testing.T) {
	expected := "http:///.well-known/ni/sha-256/UyaQV-Ev4rdLoHyJJWCi11OHfrYv9E1aGQAlMO2X_-Q"

	url, err := ParseNI(testURL)
	if err != nil {
		t.Fatal(err)
	}

	if url.WellKnownURI() != expected {
		t.FailNow()
	}
}

func TestNIH(t *testing.T) {
	url, err := ParseNI(testURL)
	if err != nil {
		t.Fatal(err)
	}
	if url.NIH() != testNIH {
		t.FailNow()
	}
}

func TestToHexString(t *testing.T) {
	expected := "0153269057e12fe2b74ba07c892560a2d753877eb62ff44d5a19002530ed97ffe4"

	url, err := ParseNI(testURL)
	if err != nil {
		t.Fatal(err)
	}

	if url.HexString() != expected {
		t.FailNow()
	}
}

func TestToBytes(t *testing.T) {
	expected := []byte{0x01, 0x53, 0x26, 0x90, 0x57, 0xe1, 0x2f, 0xe2, 0xb7, 0x4b, 0xa0, 0x7c, 0x89, 0x25, 0x60, 0xa2, 0xd7, 0x53, 0x87, 0x7e, 0xb6, 0x2f, 0xf4, 0x4d, 0x5a, 0x19, 0x00, 0x25, 0x30, 0xed, 0x97, 0xff, 0xe4}

	url, err := ParseNI(testURL)
	if err != nil {
		t.Fatal(err)
	}

	if !bytes.Equal(url.Bytes(), expected) {
		t.FailNow()
	}
}

func TestDigestToNi(t *testing.T) {
	digest := []byte{0x53, 0x26, 0x90, 0x57, 0xe1, 0x2f, 0xe2, 0xb7, 0x4b, 0xa0, 0x7c, 0x89, 0x25, 0x60, 0xa2, 0xd7, 0x53, 0x87, 0x7e, 0xb6, 0x2f, 0xf4, 0x4d, 0x5a, 0x19, 0x00, 0x25, 0x30, 0xed, 0x97, 0xff, 0xe4}

	url, err := DigestToNI(digest, "sha-256", "")
	if err != nil {
		t.Fatal(err)
	}

	if !bytes.Equal(url.valBytes, digest) {
		t.FailNow()
	}
}

func TestString(t *testing.T) {
	url, err := ParseNI(testURL)
	if err != nil {
		t.Fatal(err)
	}

	if url.String() != testURL {
		t.FailNow()
	}
}

// jarischf@muc-nb-055 ~/g/sep-aisec-sephole (refactor) [1]> ./sephole -a "ni://ace-sep.de/sha3-256;jzrM33q1Fo8yzIy60PmDNeQk0LEfqrjM_IErpqSI011" ffa
// fingerprint 'ni://ace-sep.de/sha3-256;jzrM33q1Fo8yzIy60PmDNeQk0LEfqrjM_IErpqSI011' exists
// jarischf@muc-nb-055 ~/g/sep-aisec-sephole (refactor) [1]> cat ~/.config/sephole/authorized_fingerprints
// ni://ace-sep.de/sha3-256;jzrM33q1Fo8yzIy60PmDNeQk0LEfqrjM_IErpqSI010	ff
// jarischf@muc-nb-055 ~/g/sep-aisec-sephole (refactor)>
func TestFerdiIssue(t *testing.T) {
	url1, err := ParseNI("ni://ace-sep.de/sha3-256;jzrM33q1Fo8yzIy60PmDNeQk0LEfqrjM_IErpqSI010")
	if err != nil {
		t.Fatal(err)
	}

	url2, err := ParseNI("ni://ace-sep.de/sha3-256;jzrM33q1Fo8yzIy60PmDNeQk0LEfqrjM_IErpqSI011")
	if err != nil {
		t.Fatal(err)
	}

	// The last charecter does not count.
	// https://rumpelsepp.org/2017/02/25/base64-encoder.html#padding
	if !bytes.Equal(url1.Bytes(), url2.Bytes()) {
		t.Fatal("ferdi's issue bites!!")
	}
}
