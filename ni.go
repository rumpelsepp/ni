// Package ni implements RFC6920: https://tools.ietf.org/html/rfc6920
package ni

import (
	"bytes"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"unicode/utf8"
)

var (
	ErrInvalidScheme     = errors.New("invalid scheme")
	ErrInvalidDigestLen  = errors.New("invalid digest len")
	ErrSuiteNotSupported = errors.New("suite not supported")
	ErrCheckDigit        = errors.New("wrong check digit")
)

// https://www.iana.org/assignments/named-information/named-information.xhtml
var suiteIDNames = map[uint8]string{
	0x01: "sha-256",
	0x02: "sha-256-128",
	0x03: "sha-256-120",
	0x04: "sha-256-96",
	0x05: "sha-256-64",
	0x06: "sha-256-32",
	0x07: "sha-384",
	0x08: "sha-512",
	0x09: "sha3-224",
	0x0a: "sha3-256",
	0x0b: "sha3-384",
	0x0c: "sha3-512",
}

var digestLen = map[uint8]int{
	0x01: 32,
	0x02: 16,
	0x03: 15,
	0x04: 12,
	0x05: 8,
	0x06: 4,
	0x07: 48,
	0x08: 64,
	0x09: 28,
	0x0a: 32,
	0x0b: 48,
	0x0c: 64,
}

var suiteID map[string]uint8

func init() {
	suiteID = make(map[string]uint8)

	for k, v := range suiteIDNames {
		suiteID[v] = k
	}
}

// URL represents a ni-url in memory. For the specification of the properties,
// see RFC6920: https://tools.ietf.org/html/rfc6920
type URL struct {
	Authority string
	Alg       string
	Val       string
	valBytes  []byte
	suiteID   uint8
}

// DigestToNI constructs a URL from a digest and the corresponding suite.
// Authority may be an empty string, as defined in RFC6920.
// The suite string must conform the the table here:
// https://www.iana.org/assignments/named-information/named-information.xhtml
func DigestToNI(digest []byte, suite, authority string) (*URL, error) {
	suiteID, ok := suiteID[suite]
	if !ok {
		return nil, ErrSuiteNotSupported
	}

	if len(digest) != digestLen[suiteID] {
		return nil, ErrInvalidDigestLen
	}

	val := base64.RawURLEncoding.EncodeToString(digest)

	return &URL{
		Authority: authority,
		Alg:       suite,
		Val:       val,
		valBytes:  digest,
		suiteID:   suiteID,
	}, nil
}

// ParseNI parses a string containing an ni url, e.g.
// "ni:///sha-256;UyaQV-Ev4rdLoHyJJWCi11OHfrYv9E1aGQAlMO2X_-Q" into
// a URL struct.
func ParseNI(rawurl string) (*URL, error) {
	u, err := url.Parse(rawurl)
	if err != nil {
		return nil, err
	}

	if u.Scheme != "ni" {
		return nil, ErrInvalidScheme
	}

	if u.Path == "" {
		return nil, fmt.Errorf("alg-val is empty")
	}

	algVal := strings.Split(u.Path[1:], ";")

	if len(algVal) != 2 {
		return nil, fmt.Errorf("invalid alg-val")
	}

	suite := algVal[0]
	suiteID, ok := suiteID[suite]
	if !ok {
		return nil, ErrSuiteNotSupported
	}

	val := algVal[1]

	valBytes, err := base64.RawURLEncoding.DecodeString(algVal[1])
	if err != nil {
		return nil, err
	}

	return &URL{
		Authority: u.Host,
		Alg:       suite,
		Val:       val,
		valBytes:  valBytes,
		suiteID:   suiteID,
	}, nil
}

func (u *URL) String() string {
	return fmt.Sprintf("ni://%s/%s;%s", u.Authority, u.Alg, u.Val)
}

// WellKnownURI generates a well-known http url. The mapping is specified in
// RCF6920, Section 4.
func (u *URL) WellKnownURI() string {
	return fmt.Sprintf("http://%s/.well-known/ni/%s/%s", u.Authority, u.Alg, u.Val)
}

// Referenced by RFC6920:
// https://en.wikipedia.org/w/index.php?title=Luhn_mod_N_algorithm&oldid=449928878
func luhnModSum(s string) (rune, error) {
	a := "0123456789abcdef"
	factor := 2
	sum := 0
	n := len(a)

	// Starting from the right and working leftwards is easier since
	// the initial "factor" will always be "2"
	for i := len(s) - 1; i >= 0; i-- {
		codepoint := strings.IndexByte(a, s[i])
		if codepoint == -1 {
			return 0, fmt.Errorf("digit %q not valid in alphabet %q", s[i], a)
		}

		addend := factor * codepoint

		// Alternate the "factor" that each "codePoint" is multiplied by
		if factor == 2 {
			factor = 1
		} else {
			factor = 2
		}

		// Sum the digits of the "addend" as expressed in base "n"
		addend = (addend / n) + (addend % n)
		sum += addend
	}

	// Calculate the number that must be added to the "sum"
	// to make it divisible by "n"
	remainder := sum % n
	checkCodepoint := (n - remainder) % n

	return rune(a[checkCodepoint]), nil
}

// ParseNIH parses a speakable representation of URL as specified in
// RFC6920, Section 7. If a check digit is included, it is verified.
// Authority is always an empty string in the returned URL, since
// nih strings do not carry authority information.
func ParseNIH(raw string) (*URL, error) {
	if !strings.HasPrefix(raw, "nih:") {
		return nil, ErrInvalidScheme
	}

	// Remove "nih:" prefix.
	raw = raw[4:]

	parts := strings.Split(raw, ";")
	if len(parts) != 2 && len(parts) != 3 {
		return nil, fmt.Errorf("invalid nih string")
	}

	suiteID, err := strconv.ParseUint(parts[0], 16, 8)
	if err != nil {
		return nil, err
	}

	if suiteID > 0x0f {
		return nil, ErrSuiteNotSupported
	}

	rawVal := strings.Replace(parts[1], "-", "", -1)

	digest, err := hex.DecodeString(rawVal)
	if err != nil {
		return nil, err
	}

	if len(digest) != digestLen[uint8(suiteID)] {
		return nil, ErrSuiteNotSupported
	}

	hexString := make([]byte, hex.EncodedLen(len(digest)))
	n := hex.Encode(hexString, digest)

	if len(parts) == 3 {
		computedCheck, err := luhnModSum(string(hexString[:n]))
		if err != nil {
			panic(err)
		}

		if len(parts[2]) != 1 {
			return nil, ErrCheckDigit
		}

		checkDigit, _ := utf8.DecodeRuneInString(parts[2])

		if checkDigit := checkDigit; checkDigit != computedCheck {
			return nil, ErrCheckDigit
		}
	}

	return &URL{
		Alg:      suiteIDNames[uint8(suiteID)],
		Val:      base64.RawURLEncoding.EncodeToString(digest),
		valBytes: digest,
		suiteID:  uint8(suiteID),
	}, nil
}

// NIH generates a human speakable representation of URL as specified in
// RFC6920, Section 7. A check digit is included, the Val is divided in
// groups of 6.
func (u *URL) NIH() string {
	var (
		groupedHexString strings.Builder
		windowSize       = 3
		fullGroups       = int(len(u.valBytes) / windowSize)
		remainingChars   = len(u.valBytes) % windowSize
	)

	for i := 0; i < fullGroups; i++ {
		window := u.valBytes[i*windowSize : (i+1)*windowSize]
		dst := make([]byte, hex.EncodedLen(len(window)))
		n := hex.Encode(dst, window)

		groupedHexString.Write(dst[:n])

		// Detect last loop iteration
		if i != (fullGroups-1) || remainingChars > 0 {
			groupedHexString.WriteString("-")
		}
	}

	if remainingChars > 0 {
		window := u.valBytes[fullGroups*windowSize : (fullGroups*windowSize)+remainingChars]
		dst := make([]byte, hex.EncodedLen(len(window)))
		n := hex.Encode(dst, window)

		groupedHexString.Write(dst[:n])
	}

	hexString := make([]byte, hex.EncodedLen(len(u.valBytes)))
	n := hex.Encode(hexString, u.valBytes)

	checkdigit, err := luhnModSum(string(hexString[:n]))
	if err != nil {
		panic(err)
	}

	return fmt.Sprintf("nih:%x;%s;%s", u.suiteID, groupedHexString.String(), string(checkdigit))
}

// Bytes returns the binary representation of an ni-url as specified in
// RFC6920, Section 6. This representation includes the suide ID in the
// first byte which MUST be excluded when used in cryptographic operations.
func (u *URL) Bytes() []byte {
	var buf bytes.Buffer
	buf.WriteByte(u.suiteID)
	buf.Write(u.valBytes)

	return buf.Bytes()
}

// HexString returns the binary representation of an ni-url as specified in
// RFC6920, Section 6. In contrast to ToBytes, this function returns a hex
// string instead. This representation includes the suide ID in the
// first byte which MUST be excluded when used in cryptographic operations.
func (u *URL) HexString() string {
	var buf bytes.Buffer
	buf.WriteByte(u.suiteID)
	buf.Write(u.valBytes)
	return hex.EncodeToString(buf.Bytes())
}
